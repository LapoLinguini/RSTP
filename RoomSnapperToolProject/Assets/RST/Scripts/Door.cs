using System.Linq;
using UnityEditor;
using UnityEngine;

namespace RST
{
    [RequireComponent(typeof(BoxCollider))]
    public class Door : MonoBehaviour
    {
        //LOCAL VAR
        BoxCollider col;

        Vector3 outsidePos = Vector3.zero;
        Vector3 insidePos = Vector3.zero;

        float minorColSizeAxis = 0;

        //TO USE VAR
        public Vector3 colCenter { get; private set; }
        public Vector3 doorForward { get; private set; }
        public Vector3 doorSnapPos { get; private set; }

        private void OnDrawGizmosSelected()
        {
            Init();

            //draws gizmos only if the door is selected
            if (!Selection.gameObjects.Contains(transform.gameObject)) return;

            #region Label GUIStyle

            GUIStyle style = new GUIStyle();
            style.normal.textColor = Color.black;
            style.alignment = TextAnchor.MiddleCenter;
            style.fontSize = 15;
            style.fontStyle = FontStyle.Bold;

            #endregion

            Handles.Label(outsidePos, "Outside", style);
            Handles.Label(insidePos, "Inside", style);

            Gizmos.color = Color.green;
            Gizmos.DrawSphere(doorSnapPos, .1f);
        }
        public void Init()
        {
            //finds the center of the collider in world coordinates
            col = GetComponent<BoxCollider>();
            colCenter = transform.position + transform.TransformVector(col.center);

            //finds the door's forward vector based on its X and Z sizes
            doorForward = Mathf.Abs(col.size.x) - Mathf.Abs(col.size.z) >= 0 ? transform.forward : transform.right;
            minorColSizeAxis = Mathf.Abs(col.size.x) - Mathf.Abs(col.size.z) >= 0 ? col.size.z : col.size.x;

            //finds the outside and inside middle positions of the door
            outsidePos = colCenter + doorForward * (minorColSizeAxis / 2);
            insidePos = colCenter - doorForward * (minorColSizeAxis / 2);

            //finds the point where the corresponding door will be snapped to
            doorSnapPos = outsidePos - transform.TransformVector(new Vector3(0, Mathf.Abs(col.size.y) / 2, 0));
        }
    }

#if UNITY_EDITOR
    [CustomEditor(typeof(Door)), CanEditMultipleObjects]
    public class DoorCustomEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            EditorGUILayout.HelpBox("Rotate and adjust the door collider until the Outside/Inside directions match your needs.\n \n" +
                "Remember to resize the collider in order to fit the borders of your door.\n \n" +
                "Note: The important thing is to match correctly the floor height and the width of the door", MessageType.Info);
        }
    }
#endif
}
