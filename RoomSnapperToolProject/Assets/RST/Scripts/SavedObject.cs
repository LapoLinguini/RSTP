using UnityEngine;

[System.Serializable]
public class SavedObject
{
    public Vector3 _objPos;
    public Quaternion _objRot;
    public int _objIndex;

    public SavedObject(Vector3 objPos, Quaternion objRot, int objIndex)
    {
        _objPos = objPos;
        _objRot = objRot;
        _objIndex = objIndex;
    }
}
