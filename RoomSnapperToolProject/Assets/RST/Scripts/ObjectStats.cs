using UnityEngine;

public class ObjectStats : MonoBehaviour
{
    public Vector3 _objPos { get; set; }
    public Quaternion _objRot { get; set; }
    public int _objIndex { get; set; }
}
