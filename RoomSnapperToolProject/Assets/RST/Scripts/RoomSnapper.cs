using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace RST
{
    public enum SaveMode
    {
        Scene,
        Json
    }
    public class RoomSnapper : EditorWindow
    {
        [MenuItem("Tools/RoomSnapper")]
        public static void OpenWindow() => GetWindow(typeof(RoomSnapper));

        //prefabs
        GameObject _doorTrigger = null;
        GameObject[] _roomPrefabs = null;

        //variables
        [Range(30, 100)] public int buttonsSize = 70;
        public int previewHeight = 0;

        float _sceneViewWidth = 0;
        float _sceneViewHeight = 0;
        bool _isSnapping = false;
        RST.Door[] _previewPrefabRooms = null;
        AdjacentDoors? _doorsToSnap = null;

        delegate void PreviewPrefabMovement(ref GameObject previewPrefab);
        PreviewPrefabMovement previewPrefMov;

        GameObject _selectedPrefab = null;
        GameObject _previewPrefab = null;

        public SaveMode _saveMode = SaveMode.Json;
        public TextAsset _levelToLoad = null;

        //Serialized properties
        SerializedObject so;
        SerializedProperty buttonsSizeP;
        SerializedProperty previewHeightP;
        SerializedProperty saveModeP;
        SerializedProperty levelToLoadP;

        private void OnEnable()
        {
            //events linking
            Selection.selectionChanged += Repaint;
            SceneView.duringSceneGui += DuringSceneGUI;

            //initializes serialized object & properties
            so = new SerializedObject(this);
            buttonsSizeP = so.FindProperty(nameof(buttonsSize));
            previewHeightP = so.FindProperty(nameof(previewHeight));
            saveModeP = so.FindProperty(nameof(_saveMode));
            levelToLoadP = so.FindProperty(nameof(_levelToLoad));

            //finds door trigger prefab in the asset folder
            _doorTrigger = (GameObject)AssetDatabase.LoadAssetAtPath("Assets/RST/Prefabs/DoorTrigger/RST_DoorTrigger.prefab", typeof(GameObject));

            //finds all room prefabs in the asset folder
            FindPrefabsAtPath(ref _roomPrefabs, new string[] { "Assets/RST/Prefabs/Rooms" });

            //initializes some variables
            _previewPrefabRooms = null;
            _doorsToSnap = null;
        }
        private void OnDisable()
        {
            //events linking
            Selection.selectionChanged -= Repaint;
            SceneView.duringSceneGui -= DuringSceneGUI;
        }
        private void OnGUI()
        {
            //update serialized properties
            so.Update();

            EditorGUILayout.Space(10);

            #region Utility Buttons
            using (new EditorGUILayout.VerticalScope())
            {
                using (new EditorGUILayout.HorizontalScope())
                {
                    EditorGUILayout.HelpBox("Adds a child door trigger to each selected gameObject.", MessageType.Info);

                    EditorGUILayout.HelpBox("Checks for any changes inside the 'Prefabs/Rooms' folder.", MessageType.Info);
                }
                using (new EditorGUILayout.HorizontalScope())
                {
                    using (new EditorGUI.DisabledScope(Selection.gameObjects.Length == 0))
                    {
                        if (GUILayout.Button("Add Door Trigger"))
                        {
                            foreach (GameObject selectedRooms in Selection.gameObjects)
                            {
                                //Instantiates a door for each selected room and initializes its variables
                                RST.Door door = Instantiate(_doorTrigger, selectedRooms.transform.position, Quaternion.identity, selectedRooms.transform).GetComponent<RST.Door>();
                                door.Init();
                            }
                        }
                    }
                    if (GUILayout.Button("Refresh Prefabs"))
                    {
                        FindPrefabsAtPath(ref _roomPrefabs, new string[] { "Assets/RST/Prefabs/Rooms" });
                    }
                }
            }
            #endregion

            EditorGUILayout.Space(20);

            #region Prefab Preview Height Control
            using (new EditorGUILayout.VerticalScope())
            {
                EditorGUILayout.HelpBox("Adjust this value to set the Y coordinates to which the prefabs will be spawned and moved. \n" +
                    "You can also use the - and + numpad keys to do so.", MessageType.Info);

                using (new EditorGUILayout.HorizontalScope())
                {
                    EditorGUILayout.Space(20);

                    if (GUILayout.Button("-"))
                    {
                        previewHeightP.intValue--;
                    }

                    EditorGUILayout.PropertyField(previewHeightP, GUIContent.none, false, GUILayout.MaxWidth(70));

                    if (GUILayout.Button("+"))
                    {
                        previewHeightP.intValue++;
                    }

                    EditorGUILayout.Space(20);
                }
            }
            #endregion

            EditorGUILayout.Space(20);

            #region Save And Load
            using (new EditorGUILayout.VerticalScope())
            {
                switch (saveModeP.intValue)
                {
                    case 0:
                        EditorGUILayout.HelpBox("Saves the current selected level as a new scene inside the 'RST/SavedLevels/Scenes' folder.", MessageType.Info);
                        break;
                    case 1:
                        EditorGUILayout.HelpBox("Saves the current selected level as a new Json file inside the 'RST/SavedLevels/Json' folder.\n " +
                            "You might need to refresh the Json folder to see any changes", MessageType.Info);
                        break;
                }
                using (new EditorGUILayout.HorizontalScope())
                {
                    EditorGUILayout.PropertyField(saveModeP);

                    using (new EditorGUI.DisabledScope(Selection.gameObjects.Length == 0))
                    {
                        if (GUILayout.Button("Save Level"))
                        {
                            _previewPrefab = null;
                            SaveLevel((SaveMode)saveModeP.intValue);
                        }
                    }
                }

                EditorGUILayout.Space(20);

                using (new EditorGUILayout.HorizontalScope())
                {
                    using (new EditorGUI.DisabledScope(_levelToLoad == null))
                    {
                        if (GUILayout.Button("Load From Json"))
                        {
                            SavedLevel savedLevel = JsonUtility.FromJson<SavedLevel>(_levelToLoad.text);

                            for (int i = 0; i < savedLevel._savedObjects.Length; i++)
                            {
                                GameObject spawnedRoom = Instantiate(_roomPrefabs[savedLevel._savedObjects[i]._objIndex], savedLevel._savedObjects[i]._objPos, savedLevel._savedObjects[i]._objRot);
                                ObjectStats stats = spawnedRoom.AddComponent<ObjectStats>();
                                stats._objPos = savedLevel._savedObjects[i]._objPos;
                                stats._objRot = savedLevel._savedObjects[i]._objRot;
                                stats._objIndex = savedLevel._savedObjects[i]._objIndex;
                                _levelToLoad = null;
                            }
                        }
                    }

                    EditorGUILayout.PropertyField(levelToLoadP, GUIContent.none);
                }
            } 
            #endregion

            EditorGUILayout.Space(20);

            EditorGUILayout.PropertyField(buttonsSizeP, new GUIContent("Prefabs Buttons Size"));

            if (so.ApplyModifiedProperties())
            {
                Repaint();
            }
        }
        void DuringSceneGUI(SceneView sceneView)
        {
            //handles the undo action on the prefab instance
            if (_previewPrefab == null)
            {
                _selectedPrefab = null;
                _previewPrefabRooms = null;
            }

            //gets the scene view tab width (using "GetWindow<SceneView>" bugs out the inspector for some reason)
            _sceneViewWidth = sceneView.position.width;
            _sceneViewHeight = sceneView.position.height;

            //draws GUI
            DrawGUI();

            #region Prefab Preview Height Shortcuts
            if (Event.current.keyCode == KeyCode.KeypadMinus && Event.current.type == EventType.KeyDown)
            {
                previewHeightP.intValue--;
            }
            if (Event.current.keyCode == KeyCode.KeypadPlus && Event.current.type == EventType.KeyDown)
            {
                previewHeightP.intValue++;
            }
            #endregion

            if (Event.current.keyCode == KeyCode.Space && Event.current.type == EventType.KeyDown)
            {
                //places the prefab if possible
                PlacePrefab();
            }

            if (Event.current.keyCode == KeyCode.N && Event.current.type == EventType.KeyDown)
            {
                RotatePrefabPreview(ref _previewPrefab, -90);
            }
            if (Event.current.keyCode == KeyCode.M && Event.current.type == EventType.KeyDown)
            {
                RotatePrefabPreview(ref _previewPrefab, 90);
            }

            //checks if the user is pressing ctrl to try to snap the room
            _isSnapping = (Event.current.modifiers & EventModifiers.Control) != 0;

            //moves the prefab's preview accordingly to the mouse position or to the snapped position based on the delegate
            if (_previewPrefab != null)
            {
                previewPrefMov(ref _previewPrefab);
            }

            //checks for the nearest snappable pair of doors
            if (_previewPrefabRooms != null)
            {
                _doorsToSnap = CheckForNearbyDoors();
            }

            //switches the movement behaviour of the prefab preview
            previewPrefMov = _isSnapping && _doorsToSnap != null ? TryToSnapRooms : MoveSelectedPrefab;

            if (so.ApplyModifiedProperties())
            {
                Repaint();
            }
        }
        void DrawGUI()
        {
            Handles.BeginGUI();

            Vector2 s = new Vector2(buttonsSizeP.intValue, buttonsSizeP.intValue);

            //gets half the width of the scene view
            float _halfSceneViewWidth = (_sceneViewWidth / 2) - s.x / 2;

            //makes sure all the buttons are centered
            float _firstElementPos = _roomPrefabs.Length % 2 == 0 ? _halfSceneViewWidth - (s.x / 2) - (s.x * ((_roomPrefabs.Length / 2) - 1)) : _halfSceneViewWidth - (s.x * Mathf.Floor(_roomPrefabs.Length / 2));

            Vector2 p = new Vector2(_firstElementPos, 0f);

            //builds the rect for the buttons
            Rect r = new Rect(p, s);

            for (int i = 0; i < _roomPrefabs.Length; i++)
            {
                //gets the prefabs previews
                GameObject prefab = _roomPrefabs[i];
                Texture icon = AssetPreview.GetAssetPreview(prefab);

                //the actual buttons from which the user can pick the desired prefabs to spawn
                using (new EditorGUI.DisabledScope(_roomPrefabs[i] != _selectedPrefab && _selectedPrefab != null))
                {
                    if (GUI.Button(r, new GUIContent(icon)))
                    {
                        switch (_selectedPrefab == _roomPrefabs[i])
                        {
                            case true:
                                _selectedPrefab = null;
                                DestroyImmediate(_previewPrefab);
                                _previewPrefabRooms = null;

                                break;
                            case false:
                                _selectedPrefab = _roomPrefabs[i];
                                _previewPrefab = (GameObject)PrefabUtility.InstantiatePrefab(_selectedPrefab);
                                _previewPrefab.transform.rotation = Quaternion.identity;
                                _previewPrefab.transform.position = new Vector3(0, previewHeightP.intValue, 0);

                                ObjectStats objectProperties = _previewPrefab.AddComponent<ObjectStats>();

                                objectProperties._objIndex = i;

                                _previewPrefabRooms = _previewPrefab.GetComponentsInChildren<RST.Door>();
                                Undo.RegisterCreatedObjectUndo(_previewPrefab, "Prefab Spawn");

                                break;
                        }
                    }
                }

                r.x += r.width;
            }

            Vector2 size = new Vector2(buttonsSizeP.intValue, buttonsSizeP.intValue);

            Vector2 pos1 = new Vector2(_sceneViewWidth - size.x ,_sceneViewHeight / 2 - size.x - size.x / 2);
            Vector2 pos2 = new Vector2(_sceneViewWidth - size.x, _sceneViewHeight / 2 - size.x + size.x / 2);

            Rect rect1 = new Rect(pos1, s);
            if (GUI.Button(rect1, "+ 90�"))
            {
                RotatePrefabPreview(ref _previewPrefab, 90);
            }
            Rect rect2 = new Rect(pos2, s);
            if (GUI.Button(rect2, "- 90�"))
            {
                RotatePrefabPreview(ref _previewPrefab, -90);
            }

            Handles.EndGUI();
        }
        void FindPrefabsAtPath(ref GameObject[] _prefabArray, string[] _folderPath, string _assetType = "prefab")
        {
            string[] guids = AssetDatabase.FindAssets("t:" + _assetType, _folderPath);
            IEnumerable<string> paths = guids.Select(AssetDatabase.GUIDToAssetPath);
            _prefabArray = paths.Select(AssetDatabase.LoadAssetAtPath<GameObject>).ToArray();
        }
        void MoveSelectedPrefab(ref GameObject previewPrefab) => previewPrefab.transform.position = GetMouseWorldPos();
        void TryToSnapRooms(ref GameObject previewPrefab) => previewPrefab.transform.position += _doorsToSnap.Value._adjacentDoor.doorSnapPos - _doorsToSnap.Value._currentDoor.doorSnapPos;
        Vector3 GetMouseWorldPos()
        {
            Ray ray = HandleUtility.GUIPointToWorldRay(Event.current.mousePosition);

            //builds an imaginary plane at a given height
            Plane intersectionPlane = new Plane(Vector3.up, new Vector3(0, previewHeightP.intValue, 0));

            //gets the intersection point between the ray and the plane
            if (intersectionPlane.Raycast(ray, out float distance))
            {
                return ray.GetPoint(distance);
            }
            return new Vector3(0, previewHeightP.intValue, 0);
        }
        AdjacentDoors? CheckForNearbyDoors()
        {
            //checks if the current prefabs owns at least one door
            if (_previewPrefabRooms.Length <= 0)
            {
                Debug.LogWarning("-WARNING- The current room doesn't own any door. " +
                    "Please add at least one using the 'Add Door Trigger' button on top of the inspector.");
                return null;
            }

            //creates a new list of adjacent doors (see struct below)
            List<AdjacentDoors> _adjacentDoors = new List<AdjacentDoors>();

            //foreach room inside the preview prefab
            for (int i = 0; i < _previewPrefabRooms.Length; i++)
            {
                //calls the function to update all the variables inside each door collider
                _previewPrefabRooms[i].Init();

                //gets the array of collider hits under a certain radius
                var colHits = Physics.OverlapSphere(_previewPrefabRooms[i].colCenter, 4);

                //foreach hit collider
                foreach (var col in colHits)
                {
                    //checks if the current collider has a door component, if it belongs to another room prefab and if they are both facing outwards
                    if (col.GetComponent<RST.Door>() && col.transform.parent != _previewPrefabRooms[i].transform.parent &&
                        Mathf.RoundToInt(Vector3.Dot(_previewPrefabRooms[i].doorForward, col.GetComponent<RST.Door>().doorForward)) == -1)
                    {
                        //returns the first available pair of doors and keeps iterating through each door
                        _adjacentDoors.Add(new AdjacentDoors(_previewPrefabRooms[i], col.GetComponent<RST.Door>()));
                        break;
                    }
                }
            }

            //if no nearby doors are found it returns
            if (_adjacentDoors.Count <= 0) return null;

            //creates a new array of distances
            float[] _distances = new float[_adjacentDoors.Count];

            //adds each distance from each pair of doors to the distances array
            for (int i = 0; i < _adjacentDoors.Count; i++)
            {
                float _distance = (_adjacentDoors[i]._adjacentDoor.transform.position - _adjacentDoors[i]._currentDoor.transform.position).sqrMagnitude;
                _distances[i] = _distance;
            }

            //find the index of the smallest distance
            float minValue = Mathf.Infinity;
            int index = -1;
            for (int i = 0; i < _distances.Length; i++)
            {
                if (_distances[i] < minValue)
                {
                    index = i;
                    minValue = _distances[i];
                }
            }

            //returns the pair of doors which are closer to each other
            return _adjacentDoors[index];
        }
        void RotatePrefabPreview(ref GameObject _prefabToRotate, float _degreesToRotate)
        {
            if (_prefabToRotate == null) return;

            //rotates the prefab preview of 90�
            _prefabToRotate.transform.Rotate(new Vector3(0, _degreesToRotate, 0), Space.World);
        }
        void PlacePrefab()
        {
            if (_previewPrefab == null) return;

            ObjectStats objectProperties = _previewPrefab.GetComponent<ObjectStats>();
            objectProperties._objPos = _previewPrefab.transform.position;
            objectProperties._objRot = _previewPrefab.transform.rotation;

            _selectedPrefab = null;
            _previewPrefab = null;
            _previewPrefabRooms = null;
        }
        void SaveLevel(SaveMode saveMode)
        {
            switch (saveModeP.intValue)
            {
                case 0:
                    //scene save
                    GameObject[] levelPrefs = new GameObject[0];
                    FindPrefabsAtPath(ref levelPrefs, new string[] { "Assets/RST/SavedLevels/Scenes" }, "scene");

                    Scene _currentScene = SceneManager.GetActiveScene();
                    Scene _scene = EditorSceneManager.NewScene(NewSceneSetup.EmptyScene, NewSceneMode.Additive);

                    for (int i = 0; i < Selection.gameObjects.Length; i++)
                    {
                        SceneManager.MoveGameObjectToScene(Selection.gameObjects[i], _scene);
                    }

                    EditorSceneManager.SaveScene(_scene, "Assets/RST/SavedLevels/Scenes/Level_" + (levelPrefs.Length + 1) + ".unity", true);

                    SceneManager.UnloadSceneAsync(_scene);

                    EditorSceneManager.SaveScene(_currentScene);
                    break;
                case 1:
                    //json save
                    GameObject[] levelsSaved = new GameObject[0];
                    FindPrefabsAtPath(ref levelsSaved, new string[] { "Assets/RST/SavedLevels/Json" }, "Object");

                    SavedLevel savedLevel = new SavedLevel();

                    savedLevel._savedObjects = new SavedObject[Selection.gameObjects.Length];

                    for (int i = 0; i < savedLevel._savedObjects.Length; i++)
                    {
                        ObjectStats currentObject = Selection.gameObjects[i].GetComponent<ObjectStats>();

                        savedLevel._savedObjects[i] = new SavedObject(currentObject._objPos, currentObject._objRot, currentObject._objIndex);
                    }

                    string json = JsonUtility.ToJson(savedLevel, true);
                    File.WriteAllText("Assets/RST/SavedLevels/Json/Level_" + (levelsSaved.Length + 1) + ".json", json);

                    foreach (var savedObject in Selection.gameObjects)
                        DestroyImmediate(savedObject);

                    break;
            }
        }
    }
    public struct AdjacentDoors
    {
        public RST.Door _currentDoor;
        public RST.Door _adjacentDoor;

        public AdjacentDoors(RST.Door currentDoor, RST.Door adjacentDoor)
        {
            _currentDoor = currentDoor;
            _adjacentDoor = adjacentDoor;
        }
    }
}
